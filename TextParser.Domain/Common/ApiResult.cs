﻿using System.Net;

namespace TextParser.Domain.Common
{
    public class ApiResult<T>
    {
        public HttpStatusCode HttpStatus { get; set; }
        public bool IsError { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
    }
}
