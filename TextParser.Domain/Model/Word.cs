﻿using System.Collections.Generic;

namespace TextParser.Domain.Model
{
    public class Word
    {
        public string word { get; set; }
        public int NoOfInstances { get; set; }
        public string pronunciation { get; set; }
        public IList<WordDefinition> definitions { get; set; }
    }
}
