﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using TextParser.Domain.Common;
using TextParser.Domain.Model;

namespace TextParser.Domain.Services
{
    public interface IDocumentService
    {
        Task<ApiResult<IList<Word>>> GetWordCount(IFormFile file, int toprecords = -1);
        Task<ApiResult<Word>> GetWordDefinitions(IFormFile file, string searchWord);
    }
}
