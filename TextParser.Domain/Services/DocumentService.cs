﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TextParser.Domain.Common;
using TextParser.Domain.Model;

namespace TextParser.Domain.Services
{
    public class DocumentService : IDocumentService
    {
        private readonly IOwlbotService _owlbotService;

        public DocumentService(IOwlbotService owlbotService)
        {
            _owlbotService = owlbotService;
        }

        //public async Task<ApiResult<IList<Word>>> GetWordCount(IFormFile file, int toprecords)
        //{
        //    Dictionary<string, int> dictWordCount = new Dictionary<string, int>();
        //    IList<Word> wordCount = new List<Word>();
        //    try
        //    {
        //        if (file != null && file.Length > 0)
        //        {
        //            TextReader reader = new StreamReader(file.OpenReadStream());
        //            string text = reader.ReadToEnd();
        //            char[] symbols = { ' ', '.', ',', ';', ':', '?', '\n', '\r' };
        //            string[] words = text.Split(symbols);

        //            foreach (string word in words)
        //            {
        //                if (word.Length >= 2)
        //                {
        //                    string w = word.Trim().ToLower();
        //                    if (!dictWordCount.ContainsKey(w))
        //                    {
        //                        // Add new word to Dictionary
        //                        dictWordCount.Add(w, 1);
        //                    }
        //                    else
        //                    {
        //                        // Update word instance count in Dictionary
        //                        dictWordCount[w] += 1;
        //                    }
        //                }
        //            }

        //            // Apply filters
        //            if (toprecords != -1)
        //            {
        //                dictWordCount = dictWordCount.OrderByDescending(x => x.Value).Take(toprecords).ToDictionary(o => o.Key, o => o.Value);
        //            }

        //            if (dictWordCount.Count() > 0)
        //            {
        //                // Append to WordCount List
        //                foreach (var item in dictWordCount.OrderByDescending(x => x.Value))
        //                {
        //                    wordCount.Add(new Word()
        //                    {
        //                        word = item.Key,
        //                        NoOfInstances = item.Value
        //                    });
        //                }

        //                return new ApiResult<IList<Word>>()
        //                {
        //                    Data = wordCount,
        //                    HttpStatus = HttpStatusCode.OK,
        //                    IsError = false,
        //                    Message = "Word Count found"
        //                };
        //            }
        //            else
        //            {
        //                return new ApiResult<IList<Word>>
        //                {
        //                    Data = null,
        //                    HttpStatus = HttpStatusCode.OK,
        //                    IsError = false,
        //                    Message = "No Words Found"
        //                };
        //            }
        //        }
        //        else
        //        {
        //            return new ApiResult<IList<Word>>
        //            {
        //                Data = null,
        //                HttpStatus = HttpStatusCode.NoContent,
        //                IsError = true,
        //                Message = "No File attached"
        //            };
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return new ApiResult<IList<Word>>
        //        {
        //            Data = null,
        //            HttpStatus = HttpStatusCode.InternalServerError,
        //            IsError = true,
        //            Message = ex.Message
        //        };
        //    }
        //}

        public async Task<ApiResult<IList<Word>>> GetWordCount(IFormFile file, int toprecords)
        {
            Dictionary<string, int> dictWordCount = new Dictionary<string, int>();
            IList<Word> wordCount = new List<Word>();
            try
            {
                if (file != null && file.Length > 0)
                {
                    TextReader reader = new StreamReader(file.OpenReadStream());
                    string text = reader.ReadToEnd();
                    string[] words = Regex.Split(text, @"\W+").ToArray();

                    dictWordCount = words
                                    .GroupBy(word => word)
                                    .ToDictionary(
                                     w => w.Key, 
                                     w => w.Count());             

                    // Apply filters
                    if (toprecords != -1)
                    {
                        dictWordCount = dictWordCount.OrderByDescending(x => x.Value).Take(toprecords).ToDictionary(o => o.Key, o => o.Value);
                    }

                    if (dictWordCount.Count() > 0)
                    {
                        // Append to WordCount List
                        foreach (var item in dictWordCount.OrderByDescending(x => x.Value))
                        {
                            wordCount.Add(new Word()
                            {
                                word = item.Key,
                                NoOfInstances = item.Value
                            });
                        }

                        return new ApiResult<IList<Word>>()
                        {
                            Data = wordCount,
                            HttpStatus = HttpStatusCode.OK,
                            IsError = false,
                            Message = "Word Count found"
                        };
                    }
                    else
                    {
                        return new ApiResult<IList<Word>>
                        {
                            Data = null,
                            HttpStatus = HttpStatusCode.OK,
                            IsError = false,
                            Message = "No Words Found"
                        };
                    }
                }
                else
                {
                    return new ApiResult<IList<Word>>
                    {
                        Data = null,
                        HttpStatus = HttpStatusCode.NoContent,
                        IsError = true,
                        Message = "No File attached"
                    };
                }
            }
            catch (Exception ex)
            {
                return new ApiResult<IList<Word>>
                {
                    Data = null,
                    HttpStatus = HttpStatusCode.InternalServerError,
                    IsError = true,
                    Message = ex.Message
                };
            }
        }

        public async Task<ApiResult<Word>> GetWordDefinitions(IFormFile file, string searchWord)
        {
            Dictionary<string, int> dictWordCount = new Dictionary<string, int>();
            try
            {
                if (file != null && file.Length > 0)
                {
                    TextReader reader = new StreamReader(file.OpenReadStream());
                    string text = reader.ReadToEnd();
                    char[] symbols = { ' ', '.', ',', ';', ':', '?', '\n', '\r' };
                    string[] words = text.Split(symbols);

                    foreach (string word in words)
                    {
                        if (word.Length >= 2)
                        {
                            string dictWord = word.Trim().ToLower();
                            if (!dictWordCount.ContainsKey(dictWord))
                            {
                                // Add new word to Dictionary
                                dictWordCount.Add(dictWord, 1);
                            }
                            else
                            {
                                // Update word instance count in Dictionary
                                dictWordCount[dictWord] += 1;
                            }
                        }
                    }

                    if (dictWordCount.Count() > 0)
                    {
                        var word = new Word();
                        if (dictWordCount.ContainsKey(searchWord.ToLower()))
                        {
                            word.word = searchWord;
                            word.NoOfInstances = dictWordCount[searchWord.ToLower()];
                            word.definitions = (await _owlbotService.GetWordDefinitions(searchWord)).definitions;
                        }

                        return new ApiResult<Word>()
                        {
                            Data = word,
                            HttpStatus = HttpStatusCode.OK,
                            IsError = false,
                            Message = "Word count found"
                        };
                    }
                    else
                    {
                        return new ApiResult<Word>
                        {
                            Data = null,
                            HttpStatus = HttpStatusCode.OK,
                            IsError = false,
                            Message = "No words Found"
                        };
                    }
                }
                else
                {
                    return new ApiResult<Word>
                    {
                        Data = null,
                        HttpStatus = HttpStatusCode.NoContent,
                        IsError = true,
                        Message = "No File attached"
                    };
                }
            }
            catch (Exception ex)
            {
                return new ApiResult<Word>
                {
                    Data = null,
                    HttpStatus = HttpStatusCode.InternalServerError,
                    IsError = true,
                    Message = ex.Message
                };
            }
        }
    }
}
