﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using TextParser.Domain.Model;

namespace TextParser.Domain.Services
{
    public class OwlbotService : IOwlbotService
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _httpFactory;
        public OwlbotService(IConfiguration configuration, IHttpClientFactory httpFactory)
        {
            _configuration = configuration;
            _httpFactory = httpFactory;
        }
        
        public async Task<Word> GetWordDefinitions(string searchWord)
        {
            try
            {
                var wordDefinitions = new Word() { word = searchWord };
                var owlbotUrl = Convert.ToString(_configuration["owlboturl"]) + "/" + searchWord;
                var token = Convert.ToString(_configuration["token"]);
                var _httpClient = _httpFactory.CreateClient();
                _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Token", token);
                var response = await _httpClient.GetAsync(owlbotUrl);
                var json = await response.Content.ReadAsStringAsync();
                if (!json.Contains("No definition"))
                {
                    wordDefinitions = JsonSerializer.Deserialize<Word>(json);
                }
                else
                {
                    wordDefinitions.definitions = null;
                }
                return wordDefinitions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
