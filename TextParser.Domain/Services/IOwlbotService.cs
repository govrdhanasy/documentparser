﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TextParser.Domain.Model;

namespace TextParser.Domain.Services
{
    public interface IOwlbotService
    {
        Task<Word> GetWordDefinitions(string searchWord);
    }
}
