﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TextParser.API.Controllers;
using TextParser.Domain.Common;
using TextParser.Domain.Model;
using TextParser.Domain.Services;
using Xunit;

namespace TextParser.Tests
{
    public class DocumentTestController
    {
        DocumentController _documentController;
        IDocumentService _documentService;
        IOwlbotService _owlbotService;
        IConfiguration _configuration;
        Mock<HttpMessageHandler> _mockHttpMessageHandler;

        public DocumentTestController()
        {
            var mockFactory = new Mock<IHttpClientFactory>();
            _mockHttpMessageHandler = new Mock<HttpMessageHandler>();         
            var client = new HttpClient(_mockHttpMessageHandler.Object);
            mockFactory.Setup(_ => _.CreateClient(It.IsAny<string>())).Returns(client);

            _configuration = InitConfiguration();
            _owlbotService = new OwlbotService(_configuration, mockFactory.Object);
            _documentService = new DocumentService(_owlbotService);
            _documentController = new DocumentController(_documentService);
        }

        [Fact]
        public async void Task_GetWordCount_Return_OkAllResult()
        {
            string text = "Mahng, the loon, the wild goose, Wawa," +
                           "The blue heron, the Shuh-shuh - gah," +
                           " And the grouse, the Mushkodasa !" +
                           "If still further you should ask me, " +
                           "Saying, Who was Nawadaha ? " +
                           "Tell us of this Nawadaha," +
                           "I should answer your inquiries" +
                           "Straightway in such words as follow." +
                           "In the Yale of Tawasentha, " +
                           "In the green and silent valley," +
                           "By the pleasant water - courses, " +
                           "Dwelt the singer Nawadaha. " +
                           "Round about the Indian village" +
                           "Spread the meadows and the corn - fields, " +
                           "And beyond them stood the forest," +
                           "Stood the groves of singing pine - trees, " +
                           "Green in Summer, white in Winter, " +
                           "Ever sighing, ever singing." +
                           "And the pleasant water-courses," +
                           "You could trace them through the valley,";
            IFormFile file = CreateTestFormFile("file.txt", text);
            //Act  
            var okResult = _documentController.GetWordCount(file, 10);
            //Assert  
            await Assert.IsType<Task<ApiResult<IList<Word>>>>(okResult);
            Assert.Equal(HttpStatusCode.OK.ToString(), okResult.Result.HttpStatus.ToString());
        }

        [Fact]
        public async void Task_GetWordCount_Return_NotFileFoundResult()
        {
            string text = string.Empty;
            IFormFile file = null;
            //Act  
            var okResult = _documentController.GetWordCount(file, 10);
            //Assert  
            await Assert.IsType<Task<ApiResult<IList<Word>>>>(okResult);
            Assert.Equal(HttpStatusCode.NoContent.ToString(), okResult.Result.HttpStatus.ToString());
        }

        [Fact]
        public async void Task_GetTop10WordCount_Return_OkAllResult()
        {
            string text = "Mahng, the loon, the wild goose, Wawa," +
                           "The blue heron, the Shuh-shuh - gah," +
                           " And the grouse, the Mushkodasa !" +
                           "If still further you should ask me, " +
                           "Saying, Who was Nawadaha ? " +
                           "Tell us of this Nawadaha," +
                           "I should answer your inquiries" +
                           "Straightway in such words as follow." +
                           "In the Yale of Tawasentha, " +
                           "In the green and silent valley," +
                           "By the pleasant water - courses, " +
                           "Dwelt the singer Nawadaha. " +
                           "Round about the Indian village" +
                           "Spread the meadows and the corn - fields, " +
                           "And beyond them stood the forest," +
                           "Stood the groves of singing pine - trees, " +
                           "Green in Summer, white in Winter, " +
                           "Ever sighing, ever singing." +
                           "And the pleasant water-courses," +
                           "You could trace them through the valley,";
            IFormFile file = CreateTestFormFile("file.txt", text);
            //Act  
            var okResult = _documentController.GetWordCount(file,10);
            //Assert  
            await Assert.IsType<Task<ApiResult<IList<Word>>>>(okResult);
            Assert.Equal(HttpStatusCode.OK.ToString(), okResult.Result.HttpStatus.ToString());
        }

        [Fact]
        public async void Task_GetWordDefinitionFound_Return_OkAllResult()
        {
            string text = "Mahng, the loon, the wild goose, Wawa," +
                           "The blue heron, the Shuh-shuh - gah," +
                           " And the grouse, the Mushkodasa !" +
                           "If still further you should ask me, " +
                           "Saying, Who was Nawadaha ? " +
                           "Tell us of this Nawadaha," +
                           "I should answer your inquiries" +
                           "Straightway in such words as follow." +
                           "In the Yale of Tawasentha, " +
                           "In the green and silent valley," +
                           "By the pleasant water - courses, " +
                           "Dwelt the singer Nawadaha. " +
                           "Round about the Indian village" +
                           "Spread the meadows and the corn - fields, " +
                           "And beyond them stood the forest," +
                           "Stood the groves of singing pine - trees, " +
                           "Green in Summer, white in Winter, " +
                           "Ever sighing, ever singing." +
                           "And the pleasant water-courses," +
                           "You could trace them through the valley,";
            IFormFile file = CreateTestFormFile("file.txt", text);

            _mockHttpMessageHandler.Protected()
               .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
               .ReturnsAsync(new HttpResponseMessage
               {
                   StatusCode = HttpStatusCode.OK,
                   Content = new StringContent("{\"pronunciation\": null,\"word\": \"song\",\"definitions\": [{\"type\": \"noun\",\"definition\": \"a short poem or other set of words set to music or meant to be sung.\",\"example\": \"a pop song\",\"image_url\": null,\"emoji\": null},{\"type\": \"noun\",\"definition\": \"the musical phrases uttered by some birds, whales, and insects, typically forming a recognizable and repeated sequence and used chiefly for territorial defence or for attracting mates.\",\"example\": \"all sounds were muffled except the song of the birds\",\"image_url\": null,\"emoji\": null}]}")
                }); 

            //Act  
            var okResult = _documentController.GetWordDefinition(file, "water");
            //Assert  
            await Assert.IsType<Task<ApiResult<Word>>>(okResult);
            Assert.Equal(HttpStatusCode.OK.ToString(), okResult.Result.HttpStatus.ToString());
        }

        [Fact]
        public async void Task_GetWordDefinitionNotFound_Return_OkAllResult()
        {
            string text = "Mahng, the loon, the wild goose, Wawa," +
                           "The blue heron, the Shuh-shuh - gah," +
                           " And the grouse, the Mushkodasa !" +
                           "If still further you should ask me, " +
                           "Saying, Who was Nawadaha ? " +
                           "Tell us of this Nawadaha," +
                           "I should answer your inquiries" +
                           "Straightway in such words as follow." +
                           "In the Yale of Tawasentha, " +
                           "In the green and silent valley," +
                           "By the pleasant water - courses, " +
                           "Dwelt the singer Nawadaha. " +
                           "Round about the Indian village" +
                           "Spread the meadows and the corn - fields, " +
                           "And beyond them stood the forest," +
                           "Stood the groves of singing pine - trees, " +
                           "Green in Summer, white in Winter, " +
                           "Ever sighing, ever singing." +
                           "And the pleasant water-courses," +
                           "You could trace them through the valley,";
            IFormFile file = CreateTestFormFile("file.txt", text);

            _mockHttpMessageHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent("{ 'message': 'No definition: ('}"),
                });
            //Act  
            var okResult = _documentController.GetWordDefinition(file, "Nawadaha");
            //Assert  
            await Assert.IsType<Task<ApiResult<Word>>>(okResult);
            Assert.Equal(HttpStatusCode.OK.ToString(), okResult.Result.HttpStatus.ToString());
            Assert.Null(okResult.Result.Data.definitions);
        }

        [Fact]
        public async void Task_GetWordDefinition_Return_NotFileFoundResult()
        {
            string text = string.Empty;
            IFormFile file = null;
            //Act  
            var okResult = _documentController.GetWordDefinition(file, "test");
            //Assert  
            await Assert.IsType<Task<ApiResult<Word>>>(okResult);
            Assert.Equal(HttpStatusCode.NoContent.ToString(), okResult.Result.HttpStatus.ToString());
        }

        private IFormFile CreateTestFormFile(string p_Name, string p_Content)
        {
            byte[] s_Bytes = Encoding.UTF8.GetBytes(p_Content);

            return new FormFile(
                baseStream: new MemoryStream(s_Bytes),
                baseStreamOffset: 0,
                length: s_Bytes.Length,
                name: "Data",
                fileName: p_Name
            );
        }

        public static IConfiguration InitConfiguration()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.test.json")
                .Build();
            return config;
        }
    }
}
