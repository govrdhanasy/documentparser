﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TextParser.Domain.Common;
using TextParser.Domain.Model;
using TextParser.Domain.Services;

namespace TextParser.API.Controllers
{
    [Route("document")]
    [ApiController]
    public class DocumentController : Controller
    {
        private readonly IDocumentService _documentService;

        public DocumentController(IDocumentService documentService)
        {
            _documentService = documentService;
        }


        [Route("words/count"), HttpPost]
        public async Task<ApiResult<IList<Word>>> GetWordCount(IFormFile file, int toprecords = -1)
        {
            return await _documentService.GetWordCount(file, toprecords);
        }

        [Route("words/{word}/definitions"), HttpPost]
        public async Task<ApiResult<Word>> GetWordDefinition(IFormFile file, string word)
        {
            return await _documentService.GetWordDefinitions(file, word);
        }
    }
}
